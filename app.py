from flask import Flask, render_template, request, escape
from flask_cors import CORS
from ip_details import ipInfo
import connexion
from flask_googlemaps import GoogleMaps, Map

app = connexion.App(__name__)
app.app.config.from_object('config')
GoogleMaps(app.app)

# @app.route('/', methods=['POST', 'GET'])
def index():
    return render_template('base.html', context=None) # empty context required as used in base.html

def submitip():
    context = {}
    context['addr'] = request.form['ip_addr']
    context['ip_det'] = ipInfo(context['addr'])
    lat, lng = map(float, context['ip_det']['loc'].split(','))
    context['mymap'] = Map(
        identifier="view-side",
        lat=lat,
        lng=lng,
        markers=[(lat, lng)]
    )
    print(context)
    return render_template('base.html', context=context)

# @app.route('/d/<ip_addr>', methods=['GET'])
def details(IP):
    ip_details = ipInfo(escape(IP))
    return ip_details

# app = Flask(__name__)
app.add_api('swagger.yml')
application = app.app
cors = CORS(application)


if __name__ == "__main__":
    app.run(port=5000)
